---
author: wdullaer
title: Selfreflection
image: /img/cover_2.png
menu: main
comments: false
index: true
---
I'm Wouter Dullaert.

I'm an architect at [Tenforce](https://www.tenforce.com), where I spend a lot of time designing and implementing data processing pipelines. I've previously held similar positions at AB Inbev and Toyota Motor Europe.

In my spare time I enjoy tinkering with web technology, [Android](https://play.google.com/store/apps/details?id=com.wdullaer.mnemonic), music production, [podcast mixing](https://defn.audio), playing jazz and running.

Whenever possible, I spend time away from home with my partner to see as much of the world as we can.

This blog serves as a notebook for some of the techie problems I've solved over the years. I have added the content I've created on <http://soundhacker.be> and <http://olezfdtd.wordpress.com> to this blog and I intend to cover more subjects in the future.

Background image: [Just The Two of Us](http://lildutchindian.deviantart.com/art/Just-The-Two-of-Us-165185666) by [Peppercookie](http://peppercookie.deviantart.com/)
