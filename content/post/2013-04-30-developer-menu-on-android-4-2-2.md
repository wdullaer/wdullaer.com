---
author: wdullaer
categories:
- Android
- Tips
comments: true
date: 2013-04-30T09:31:07Z
image: /media/2013-04-30-developer-menu-on-android-4-2-2/cover.jpg
quote: Don't worry, google's just playing hide and seek
tags:
- 4.2
- android
- developer
- galaxy-nexus
- jellybean
- menu
- nexus4
- options
- settings
title: Developer menu on Android 4.2+
url: /blog/2013/04/30/developer-menu-on-android-4-2-2/
wordpress_id: 12
---

If you recently bought a Nexus 4 or upgraded your phone to Android 4.2 you'll probably be missing the "Developer options" in your settings menu.
Fortunately, Google didn't remove the developer options from their new version of Android, they just hid it from prying eyes.

If you **press 5 times** on the `Build-number` in the `About phone` menu, your phone will recognise you as a developer and the `Developer options` menu will magically reappear.

{% comment %}Replace this with an English language animated gif{% endcomment %}
[![About Phone Menu](/media/2013-04-30-developer-menu-on-android-4-2-2/about_phone_menu.png)](/media/2013-04-30-developer-menu-on-android-4-2-2/about_phone_menu.png)
