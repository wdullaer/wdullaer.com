---
author: wdullaer
comments: true
date: 2016-07-17T00:00:00Z
excerpt: "A bit over a year and a half ago I moved my blog to Github pages and Jekyll. The promise of no hassle/maintenance blogging that static website generators offer (combined with, let's be honest, free hosting) was very luring. By and large this setup has delivered on its promise, but it was definitely not perfect. Recently the ever present desire to improve (and experiment) got the best of me and I started working on the backend of this blog again. This post tries to describe all the changes I've done, and why I've done them."
quote: Now with more https
tags:
- hugo
- cloudflare
- gitlab
title: The New and Improved wdullaer.com
video: false
---
A bit over a year and a half ago I moved my blog to Github pages and Jekyll. The promise of no hassle/maintenance blogging that static website generators offer (combined with, let's be honest, free hosting) was very luring. By and large this setup has delivered on its promise, but it was definitely not perfect. Recently the ever present desire to improve (and experiment) got the best of me and I started working on the backend of this blog again. This post tries to describe all the changes I've done, and why I've done them.

## Switch from Jekyll to Hugo
As one of the first static website generators, Jekyll helped pioneer the idea that could run a blog without a massive amount of runtime dependencies. It is used by lots of people successfully, but there were enough annoying little corners that got me looking for an alternative:

* Ruby made it rather hard to run Jekyll locally, both on the Mac and Linux. I eventually put it all in docker.
* Installing the gems seem to require root permissions.
* Github at some point changed their trailing slash behaviour which broke the disqus comments.
* Github pages keeps on moving things from included to explicit modules, or renaming things. This meant that things started breaking at funny times and I spent more time tracking the dependencies than I did with Wordpress.
* I found it rather hard to extend.

When Github pages decided to deprecate the markdown renderer I was using, and the new one broke my source code blocks, I decided to try out hugo.

Hugo comes with a nice helper function that migrates your content across:

```bash
hugo import jekyll /my/jekyll/blog /my/hugo/blog
```

All that was left to do now was install a nice theme in `/themes`. In all of five minutes I had ported my blog, gained support for tags (something I've been wanting to add for a while), properly rendering code blocks and no messing around with plugins or dependencies. Everything just worked out of the box.

I did spend quite some time converting my old jekyll theme, but that really wasn't necessary.


## Move from Github Pages to Gitlab Pages
Github pages is a really nice service, but it only supports building Jekyll. I could render the blog locally on my machine and then pushing the result into a separate git repository to be hosted, but I'm really fond of the current workflow. There is something really pleasing about doing a git push and having your content built and rendered (as people using Heroku will confirm).

Fortunately, I'm in luck and [Gitlab](https://gitlab.com), an open source competitor of Github, has recently announced their static website hosting, called Gitlab Pages (how original). It exposes a little bit more of the internal wiring as Github Pages does, but this brings with it a lot more power.

Basically you just add a `.gitlab-ci.yml` file to your project which describes how the content is rendered and where the artifact (in this case the static site) can be picked up. They have a lot of ready to go configurations for what seems like every static website generator under the sun at [https://gitlab.com/pages](https://gitlab.com/pages). Whenever they detect a change in your project, they'll fire up a free CI worker, build the project and host the results for you.

To round things out they also make it easy to add a custom domain, to your project and configure certificates, so you can serve it over https (something you can't do with Github pages). They're also [looking at adding Let's Encrypt support](https://gitlab.com/gitlab-org/gitlab-ee/issues/474) in the future.

All in all, it was just as easy to set up as Github Pages, but I really prefer the added flexibility over the "we'll take care of it, until we don't" magic of Github Pages.

## Use Cloudflare for DNS
For the new version of this blog, I wanted to run it at the apex domain. `http://blog.wdullaer.com/blog` had too much blog in it and `http://www.wdullaer.com` is just too many w's (quadruple double u's?). Unfortunately my registrar Namecheap doesn't have support for CNAME like functionality at the apex. (Some weird paragraph in the DNS spec makes putting a real CNAME at the apex something which will cause issues in weird and wonderful ways). After reading [this post](https://blog.cloudflare.com/introducing-cname-flattening-rfc-compliant-cnames-at-a-domains-root/) post about CNAME flattening, I decided to move my DNS server to Cloudflare's free offering.

Apart from the CNAME flattening, Cloudflare offers a lot of nice features:

* I can put a CNAME on the apex and still have email
* I can very easily create redirects (namecheap DNS supported this as well)
* I can very easily enable http to https redirects. Cloudflare even provides a free certificate. (The downside of their DNS offering is that they decrypt at the edge of their network to provide the CDN services, so don't rely on this if you're building a banking platform)
* I regain a CDN for my content that I lost by moving from Github to Gitlab pages

The only thing I lost was the email forwarding functionality Namecheap offers, but this can be fixed by moving mail into Google Apps or using a service like [Mailgun](https://mailgun.com)

The free service of Cloudflare is nothing short of amazing in my opinion.

## Conclusion
I'm pretty happy with the new setup. Now the only thing I need to do is create some more content. Should be easy right?
