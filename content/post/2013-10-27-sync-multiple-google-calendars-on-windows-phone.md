---
author: wdullaer
categories:
- Tips
comments: true
date: 2013-10-27T17:43:00Z
excerpt: "Windows Phone relies on Google Sync (ActiveSync) to get your Google Calendars
  on your device. By default it only syncs your personal calendar. Here's how you
  can change that."
image: /media/2013-10-27-sync-multiple-google-calendars-on-windows-phone/cover.jpg
quote: A dive into the more arcane Google settings
tags:
- activesync
- google-calendar
- google-sync
- windowsphone
title: Sync Multiple Google Calendars on Windows Phone
aliases:
- /blog/2013/10/27/sync-multiple-google-calendars-on-windows-phone/
video: false
wordpress_id: 77
---

Windows Phone relies on Google Sync (ActiveSync) to get your Google Calendars on your device. By default it only syncs your personal calendar. Here's how you can change that.

## Introduction
It's a very public secret that Google's popular webservices don't play very nice with Microsoft's Windows Phone operating system. Windows Phone uses ActiveSync to get your email, contacts and calendar on the phone. While this works as advertised for mail and contacts (for the time being, because Google is [pulling the plug on Active sync support](http://support.google.com/a/answer/2716936) for normal accounts soon), by default this will only sync your personal Google Calendar. Even though there are no options in Windows Phone to change this, it is possible to sync multiple calendars.

## Let's fix it
Windows Phone offers no way to select which calendars of your google account you want to sync. You have to select this in your Google Sync settings, Google's name for its Active Sync implementation.

1. From your Windows Phone you should go to [http://m.google.com/sync/settings](http://m.google.com/sync/settings)  
[![Select Device](/media/2013-10-27-sync-multiple-google-calendars-on-windows-phone/wp_ss_20131027_0002.jpg)](/media/2013-10-27-sync-multiple-google-calendars-on-windows-phone/wp_ss_20131027_0002.jpg)

2. Select your device from the list and put a check the box next to the calendars you would like to sync.  
[![Select Calendars](/media/2013-10-27-sync-multiple-google-calendars-on-windows-phone/wp_ss_20131027_0003.jpg)](/media/2013-10-27-sync-multiple-google-calendars-on-windows-phone/wp_ss_20131027_0003.jpg)

3. Click on "save" and the next time your smartphone syncs with Google, the new calendars should appear.
