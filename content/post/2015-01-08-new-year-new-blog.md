---
author: wdullaer
comments: true
date: 2015-01-08T00:00:00Z
quote: Less maintenance, more content!
title: 'New Year: New Blog'
aliases:
- /blog/2015/01/08/new-year-new-blog/
video: false
---

I have been running a blog in one form or another for a multitude of years. The past years I've mostly been using wordpress, but the maintenance was becoming very annoying.
There are also very limited offline writing options. In short, it was time for something different and because I was already creating my drafts in markdown, jekyll seemed like a good choice.

Currently it's still a bit empty here, but I intend to move over the posts from my other blogs over the coming weeks.

Stay tuned ...
