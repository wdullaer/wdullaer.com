---
author: wdullaer
comments: true
date: 2016-08-17T00:00:00Z
excerpt: Normally when you want to boot from a USB stick you have to format it and 'burn' an image onto it. It is possible to make multiboot usb sticks that allow you to boot ISO image file directly. Not all live system images, like the Ubuntu desktop images, play nice with the method outlined in that post. Here is an alternative method that will allow you to boot images like Ubuntu desktop as an ISO from a multiboot stick.
quote: Bootloader inception
image: /media/2016-08-17-boot-live-disks-as-iso/cover.jpg
category:
- Tutorial
tags:
- grub4dos
- iso
- multiboot
- linux
- ubuntu
title: Boot Live Disks from a Multiboot USB as an ISO
video: false
---
[A previous post]({{< relref "2010-02-26-boot-iso-files-from-usb-with-grub4dos.md" >}}) explained how you can make a multiboot stick that can boot ISO files using grub4dos. You just need to copy the ISO image to the disk and add an entry to `/boot/menu.lst` that looks roughly like this:

```bash
title Ubuntu Mini Installer
find --set-root /images/ubuntu-mini.iso
map --heads=0 --sectors-per-track=0 /images/ubuntu-mini.iso (0xff)
map --hook
chainloader (0xff)
```

What this does is find the ISO, extract it, loopmount it in memory (first three lines) and then use a grub4dos trick by chainloading the bootloader of the image it just extracted. Typically this will load the instance of grub shipped with the ISO, which then boots the kernel. The advantage is that you don't need to know where all files are located inside the ISO image, this is handled by the bootloader of the image itself.

"Fatter" live images typically ship the filesystem as a compressed image and play a similar trick to give you a working system. However if you run these images with the inception chain of bootloaders, the references to the various file systems get mixed up and the ISO will fail to boot with "Unable to find a medium containing a live file system".

The solution is to not chainload the next bootloader, but have grub4dos directly boot the linux kernel of the ISO. This can require a bit of trial and error, because every distribution seems to have their own convention on where the files for their live images go (in `/live` or `/casper` or `/boot` or ...). In order to boot a Ubuntu Desktop ISO, the `menu.lst` entry looks like this:

```bash
title Ubuntu Desktop Live
find --set-root /images/ubuntu-desktop.iso
map --heads=0 --sectors-per-track=0 /images/ubuntu-desktop.iso (0xff) || map --mem /images/ubuntu-desktop.iso (0xff)
map --hook
root (0xff)
kernel /casper/vmlinuz.efi  file=/preseed/ubuntu.seed noprompt boot=casper iso-scan/filename=/image/ubuntu-desktop.iso quiet splash --
initrd /casper/initrd.lz
```

The differences with before are:

* `chainloader (0xff)` was replaced with `root (0xff)`: Rather than chainloading the bootloader of the `(0xff)` temp volume, the temp volume is set as the root volume for the next commands
* kernel line: This is the kernel line from the grub instance on the ISO, with an added `iso-scan/filename` option
* initrd line: This is the initrd line from the grub instance on the ISO

One way to figure out which options to pass to the kernel (and where all files are located) is to first chainload the included bootloader and inspect its boot configuration. You can then copy these parameters to the grub4dos `menu.lst`. Even though these settings differ between various distributions, the distributions themselves don't change them very often. Once you have a working entry for your favourite distro, chances are you can wholesale reuse them when the next version comes out.

## Further reading
* http://reboot.pro/topic/14644-gparted-in-grub4dos/
* http://askubuntu.com/questions/509207/how-to-boot-ubuntu-14-04-with-grub4dos-rmprepusb
