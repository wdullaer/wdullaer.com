---
author: wdullaer
comments: true
date: 2010-02-25T15:06:01Z
title: Hello world!
aliases:
- /blog/2010/02/25/hello-world/
wordpress_id: 1
---

> This article originally appeared on <http://olezfdtd.wordpress.com>
> I've copied it over to my current blog to consolidate all my blogging efforts over the years in one place.

Welcome to this blog. We're two phd students maintaining a bunch of computers in the lab. This blog just serves as our notebook. Instead of wondering "how the hell did we solve this last time" each time something goes wrong, we'll just post it here. If you find these posts useful, let us know in the comments.
