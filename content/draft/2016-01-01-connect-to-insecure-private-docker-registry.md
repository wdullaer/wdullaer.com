---
author: wdullaer
categories:
- Tutorial
comments: true
date: 2016-01-01T00:00:00Z
draft: true
excerpt: Docker only allows you to connect to private docker repositories over SSL.
  However if you quickly set up a registry for dev purposes, configuring SSL is more
  hassle than it's worth. There is a setting on the Docker daemon to allow unsecured
  connections to specific private repositories, but it is well hidden.
image: /media/2015-04-05-Hack-WD-Greens/cover.jpg
quote: Docker requires a system level tweak to allow you to connect without SSL
tags:
- Docker
- SSL
- Linux
title: Connect to a Private Docker Repository over HTTP
url: /2016/01/01/connect-to-insecure-private-docker-registry/
video: false
---

In `/etc/default/docker`
```bash
DOCKER_OPTS="--insecure-registry localhost:5000"
```

## Additional reading
<http://docs.docker.com/installation/ubuntulinux>
